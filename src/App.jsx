import "./App.css";
import Card from "./components/Card";

function App() {
  let someObj = {
    name: "Alok",
    age: 19
  }

  let myArr = [1, 2, 3, 4, 5]

  return (
    <>
      <Card name="Winter Jacket" price="166.2" />
      <Card name="Summer Wear" price="182.9" />
    </>
  );
}

export default App;
